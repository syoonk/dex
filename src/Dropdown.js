import React, { useState } from "react";

function Dropdown({onSelect, activeItem, items}) {
  const [dropdownVisible, setDropdownVisible] = useState(false);

  const clickSelectItemHandle = (_e, _item) => {
    _e.preventDefault();
    setDropdownVisible(!dropdownVisible);
    onSelect(_item);
  }

  return (
    <div className="dropdown ml-3">
      <button 
        className="btn btn-secondary dropdown-toggle" 
        type="button"
        onClick={() => setDropdownVisible(!dropdownVisible)}
      >{activeItem.label}</button>
      <div className={`dropdown-menu ${dropdownVisible ? 'visible' : ''}`}>
        {items && items.map((item, idx) => (
          <a 
            className={`dropdown-item ${item.value === activeItem.value ? 'active' : null}`} 
            href="#"
            key={idx}
            onClick={e => clickSelectItemHandle(e, item.value)}
          >{item.label}</a>
        ))}
      </div>
    </div>
  )
}

export default Dropdown;